<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;
use Storage;
use Str;

class Download extends Model
{
    use RoutesWithFakeIds;

    /**
     * Libro / artículo al que pertenece la descarga.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(File::class);
    }

    /**
     * Redirects de la descarga.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function redirects()
    {
        return $this->morphMany(Redirect::class, 'redirectable');
    }

    /**
     * Devuelve la URL desde la que se ve la descarga.
     * Se utiliza para los redirects.
     *
     * @return string
     */
    public function getShowUrl()
    {
        return route('downloads.show', [$this, $this->original_name], false);
    }

    /**
     * Fake ID attribute.
     *
     * @return mixed
     */
    public function getFakeIdAttribute()
    {
        return $this->getRouteKey();
    }

    /**
     * Sube archivos o los descarga a partir de URLs y borra los eliminados.
     *
     * @param      $request
     * @param File $file
     *
     * @return bool
     * @throws \Exception
     */
    public static function store($request, File $file)
    {
        DB::beginTransaction();
        try {
            // Borra los archivos eliminados
            $oldDownloads = $file->downloads()->pluck('name')->toArray();
            $newDownloads = [];
            foreach ($request->input('downloads') as $download) {
                if (! empty($download['name'])) {
                    $newDownloads[] = $download['name'];
                }
            }
            $deleteDownloads = array_diff($oldDownloads, $newDownloads);
            foreach ($deleteDownloads as $downloadName) {
                $deleteDownload = self::firstWhere('name', $downloadName);
                if ($deleteDownload) {
                    $delete = $deleteDownload->eliminar();
                    if ($delete !== true) {
                        throw $delete;
                    }
                }
            }

            // Sube los archivos
            foreach ($request->input('downloads') as $i => $requestDownload) {
                $upload = self::upload($requestDownload, $file, $request->file('downloads.' . $i . '.name'));
                // Si se produjo un error al subir el archivo
                if ($upload !== true) {
                    throw $upload;
                }
            }

            // Commit
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return $e;
        }
    }

    /**
     * Sube el archivo o lo descarga a partir de una URL.
     *
     * @param      $requestDownload
     * @param File $file
     * @param null $download
     *
     * @return \Exception
     * @throws \Exception
     */
    public static function upload($requestDownload, File $file, $download = null)
    {
        DB::beginTransaction();
        try {
            if (isset($requestDownload['url'])) {
                // Nombre del archivo
                $parsedName = pathinfo(parse_url(urldecode($requestDownload['url']))['path']);
                $originalName = $parsedName['basename'];

                // Chequea si ya existe
                if (self::where('original_name', $originalName)->exists()) {
                    throw new \Exception('El archivo "' . $originalName . '" ya existe.');
                }

                // Descarga el archivo
                $download = tempnam(sys_get_temp_dir(), 'tmp');

                // Si se pudo descargar el archivo
                if (copy($requestDownload['url'], $download)) {
                    // Crea un File de Laravel
                    $download = new \Illuminate\Http\File($download);
                    $extension = $parsedName['extension'] ?? $download->extension();
                } else {
                    throw new \Exception('No se pudo descargar el archivo "' . $originalName . '".');
                }
            } elseif ($download) {
                // Nombre del archivo
                $originalName = $download->getClientOriginalName();
                $extension = $download->getClientOriginalExtension();

                // Chequea si ya existe
                if (self::where('original_name', $originalName)->exists()) {
                    throw new \Exception('El archivo "' . $originalName . '" ya existe.');
                }
            }

            // Guarda el archivo en la DB
            if ($download) {
                // Resta la cantidad de caracteres de la extensión al largo total del nombre
                $length = 38 - strlen($extension);

                // Chequea que el nombre no se repita
                do {
                    $name = Str::random($length) . '.' . $extension;
                } while (Storage::exists($name));

                // Si se pudo mover el archivo al storage
                if (Storage::putFileAs('', $download, $name)) {
                    $download = new self;
                    $download->name = $name;
                    $download->original_name = $originalName;
                    $file->downloads()->save($download);
                }
            }

            // Redirects
            $download = ($download instanceof self) ? $download : self::firstWhere('name', $requestDownload['name']);
            if ($download) {
                $redirects = Redirect::store($requestDownload['redirects'], $download);
                if ($redirects !== true) {
                    throw $redirects;
                }
            }

            // Commit
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return $e;
        }
    }

    /**
     * Elimina una descarga.
     *
     * @return array
     * @throws \Exception
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina el archivo
            if (! Storage::delete($this->name)) {
                throw new \Exception('No se pudo eliminar el archivo "' . $this->original_name . '".');
            }

            // Elimina los redirects
            $this->redirects()->delete();

            // Elimina la descarga de la DB
            $this->delete();

            // Commit
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return $e;
        }
    }
}
