<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escritura extends Model
{
    protected $casts = [
        'padres' => 'array',
        'hijos' => 'array',
        'tags' => 'array'
    ];
    
}
