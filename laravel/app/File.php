<?php

namespace App;

use App\Http\Requests\StoreFile;
use DB;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;
use Storage;

class File extends Model
{
    use RoutesWithFakeIds, Sluggable;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['fullTitle'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'files' => 'collection',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['fullTitle'],
            ],
        ];
    }

    /**
     * Tipo de archivo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(FileType::class, 'file_type_id');
    }

    /**
     * Grupos que tienen el archivo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class)->withPivot('reading');
    }

    /**
     * Descargas asociadas al archivo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function downloads()
    {
        return $this->hasMany(Download::class);
    }

    /**
     * Redirects del archivo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function redirects()
    {
        return $this->morphMany(Redirect::class, 'redirectable');
    }

    /**
     * Devuelve la URL desde la que se ve el archivo.
     * Se utiliza para los redirects.
     *
     * @return string
     */
    public function getShowUrl()
    {
        return route('files.show', [$this, $this->slug], false);
    }

    /**
     * Fake ID attribute.
     *
     * @return mixed
     */
    public function getFakeIdAttribute()
    {
        return $this->getRouteKey();
    }

    /**
     * Devuelve el título completo del archivo (autor + título)
     *
     * @return string
     */
    public function getFullTitleAttribute()
    {
        return $this->author ? $this->author . ' - ' . $this->title : $this->title;
    }

    /**
     * Devuelve las URLs de todos sus archivos.
     *
     * @return array
     */
    public function getFilesUrlsAttribute()
    {
        $urls = [];

        // Si tiene descargas
        if ($this->downloads->count()) {
            foreach ($this->downloads as $download) {
                $urls[] = [
                    'url'  => route('downloads.show', [$download, $download->original_name]),
                    'text' => $download->original_name,
                ];
            }
        } else {
            // Sino se asume que es un texto
            $urls[] = [
                'url'  => route('files.show', [$this, $this->slug]),
                'text' => $this->slug,
            ];
        }

        return $urls;
    }

    /**
     * Grupos publicados.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    /**
     * Devuelve el listado de archivos formateado para vue-tables-2.
     *
     * @link https://matanya.gitbook.io/vue-tables-2/options-api
     *
     * @return array
     */
    public static function table()
    {
        $table = [
            'columns' => [
                'file_type',
                'author',
                'title',
                'published',
                'buttons',
            ],
            'items'   => [],
            'options' => [
                'headings'         => [
                    'file_type' => 'Tipo',
                    'author'    => 'Autor',
                    'title'     => 'Título',
                    'published' => 'Publicado',
                    'buttons'   => '',
                ],
                'columnsClasses'   => [
                    'published' => 'text-center',
                    'buttons'   => 'table-btns',
                ],
                'sortable'         => [
                    'file_type',
                    'author',
                    'title',
                    'published',
                ],
                'skin'             => 'table table-hover',
                'resizableColumns' => false,
                'perPage'          => 50,
            ],
        ];

        // Items
        $files = self::with(['type', 'downloads'])->orderBy('slug')->get();
        foreach ($files as $file) {
            $table['items'][] = [
                'fakeId'    => $file->fakeId,
                'file_type' => $file->type->name,
                'author'    => $file->author,
                'title'     => $file->title,
                'slug'      => $file->slug,
                'published' => $file->published,
                'showUrls'  => $file->filesUrls,
                'editUrl'   => route('admin.files.edit', $file),
            ];
        }

        return $table;
    }

    /**
     * Crea o actualiza un archivo.
     *
     * @param StoreFile $request
     * @param File|null $file
     *
     * @return array
     * @throws \Exception
     */
    public static function store(StoreFile $request, self $file = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($file) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $file = new self;
                $msg = 'creó';
                $update = false;
            }

            if ($request->input('actionType') == 'importLinks') {
                // Importación de archivos o libros a partir de links
                foreach ($request->input('files') as $requestFile) {
                    // Campos
                    $file = new self;
                    $file->file_type_id = $requestFile['file_type_id'];
                    $file->author = $requestFile['author'];
                    $file->title = $requestFile['title'];
                    $file->published = $requestFile['published'];
                    $file->save();

                    // Sube el archivo
                    $download = Download::upload($requestFile, $file);
                    if ($download !== true) {
                        throw $download;
                    }
                }

                $message = 'Los archivos se subieron correctamente.';
            } else {
                // creación/edición de un texto
                // Campos
                $file->file_type_id = $request->input('file_type_id');
                $file->author = $request->input('author');
                $file->title = $request->input('title');
                $file->published = $request->input('published', 0);
                $file->save();

                // Downloads o texto
                $fileType = FileType::findOrFail($request->input('file_type_id'));
                if ($fileType->name == 'Artículo' || $fileType->name == 'Libro') {
                    // Sube los archivos
                    $download = Download::store($request, $file);
                    if ($download !== true) {
                        throw $download;
                    }
                } else {
                    $file->text = $request->input('text');
                    $file->save();

                    // Redirects
                    $redirects = Redirect::store($request->input('redirects'), $file);
                    if ($redirects !== true) {
                        throw $redirects;
                    }
                }

                $message = '"' . $file->fullTitle . '" se ' . $msg . ' correctamente.';
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => $message,
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Elimina un archivo.
     *
     * @return array
     * @throws \Exception
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina las descargas asociadas
            foreach ($this->downloads as $download) {
                $delete = $download->eliminar();
                if ($delete !== true) {
                    throw $delete;
                }
            }

            // Elimina los redirects
            $this->redirects()->delete();

            // Elimina el archivo
            $this->delete();

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El archivo "' . $this->fullTitle . '" se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
