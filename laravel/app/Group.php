<?php

namespace App;

use App\Http\Requests\StoreGroup;
use DB;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;

class Group extends Model
{
    use RoutesWithFakeIds, Sluggable;

    public $path = 'img/groups/'; // Path de las imágenes

    private $uploadFiles = 'image'; // Archivos asociados al modelo

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['imageUrl'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'fullTitle',
            ],
        ];
    }

    /**
     * Tareas.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * Tipo de grupo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(GroupType::class, 'group_type_id');
    }

    /**
     * Archivos que tiene el grupo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files()
    {
        return $this->belongsToMany(File::class)->withPivot('reading');
    }

    /**
     * Redirects del grupo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function redirects()
    {
        return $this->morphMany(Redirect::class, 'redirectable');
    }

    /**
     * Devuelve la URL desde la que se ve el grupo.
     * Se utiliza para los redirects.
     *
     * @return string
     */
    public function getShowUrl()
    {
        return route('groups.show', [$this, $this->slug], false);
    }

    /**
     * Fake ID attribute.
     *
     * @return mixed
     */
    public function getFakeIdAttribute() {
        return $this->getRouteKey();
    }

    /**
     * URL de la imagen.
     *
     * @return string|null
     */
    public function getImageUrlAttribute()
    {
        if ($this->image) {
            return asset($this->path . $this->image);
        }

        return null;
    }

    /**
     * Grupos publicados.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    /**
     * Grupos actuales.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrent($query)
    {
        return $query->where('current', 1);
    }

    /**
     * Devuelve el título completo del grupo (año + título)
     *
     * @return string
     */
    public function getFullTitleAttribute()
    {
        return $this->year ? $this->year . ' - ' . $this->title : $this->title;
    }

    /**
     * Devuelve el listado de grupos formateado para vue-tables-2.
     *
     * @link https://matanya.gitbook.io/vue-tables-2/options-api
     *
     * @return array
     */
    public static function table()
    {
        $table = [
            'columns' => [
                'group_type',
                'year',
                'title',
                'description',
                'notes',
                'image',
                'current',
                'published',
                'buttons',
            ],
            'items'   => [],
            'options' => [
                'headings'         => [
                    'group_type'  => 'Tipo',
                    'year'        => 'Año',
                    'title'       => 'Título',
                    'description' => 'Descripción',
                    'notes'       => 'Apuntes',
                    'image'       => 'Imagen',
                    'current'     => 'Actual',
                    'published'   => 'Publicado',
                    'buttons'     => '',
                ],
                'columnsClasses'   => [
                    'description' => 'td-truncate',
                    'notes'       => 'text-center',
                    'image'       => 'text-center',
                    'current'     => 'text-center',
                    'published'   => 'text-center',
                    'buttons'     => 'table-btns',
                ],
                'columnsDisplay'   => [
                    'group_type'  => 'min_tabletL',
                    'notes'       => 'min_tabletL',
                    'image'       => 'min_tabletL',
                    'current'     => 'min_tabletL',
                    'description' => 'min_tabletL',
                ],
                'sortable'         => [
                    'group_type',
                    'year',
                    'title',
                    'description',
                    'notes',
                    'image',
                    'current',
                    'published',
                ],
                'skin'             => 'table table-hover',
                'resizableColumns' => false,
                'perPage'          => 50,
            ],
        ];

        // Items
        $groups = self::with('type')->orderByDesc('year')->orderBy('title')->get();
        foreach ($groups as $group) {
            $table['items'][] = [
                'fakeId'      => $group->fakeId,
                'group_type'  => $group->type->name,
                'title'       => $group->title,
                'slug'        => $group->slug,
                'year'        => $group->year,
                'description' => strip_markdown($group->description),
                'schedule'    => $group->schedule,
                'place'       => $group->place,
                'notes'       => (bool) $group->notes,
                'image'       => (bool) $group->image,
                'current'     => $group->current,
                'published'   => $group->published,
                'showUrl'     => route('groups.show', [$group, $group->slug]),
                'editUrl'     => route('admin.groups.edit', $group),
            ];
        }

        return $table;
    }

    /**
     * Crea o actualiza un grupo.
     *
     * @param StoreGroup $request
     * @param Group|null $group
     *
     * @return array
     * @throws \Exception
     */
    public static function store(StoreGroup $request, self $group = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($group) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $group = new self;
                $msg = 'creó';
                $update = false;
            }

            // Campos
            $group->group_type_id = $request->input('group_type_id');
            $group->title = $request->input('title');
            $group->schedule = $request->input('schedule');
            $group->place = $request->input('place');
            $group->description = $request->input('description');
            $group->notes = $request->input('notes');
            $group->year = $request->input('year');
            $group->current = $request->input('current', 0);
            $group->published = $request->input('published', 0);

            // Imagen
            $uploadedFiles = uploadFiles($group, $group->uploadFiles, $request, $update);

            $group->save();

            // Archivos
            $files = [];
            foreach ($request->input('mainFiles', []) as $fileId) {
                $files[$fileId] = ['reading' => 1];
            }
            foreach ($request->input('secondaryFiles', []) as $fileId) {
                $files[$fileId] = ['reading' => 0];
            }
            $group->files()->sync($files);

            // Tareas
            $newTasks = [];
            $oldTasks = $group->tasks()->pluck('id')->toArray();
            foreach ($request->input('tasks', []) as $i => $requestTask) {
                $task = Task::findOrNew($requestTask['id']);
                $task->description = $requestTask['description'];
                $task->dateDmy = $requestTask['dateDmy'];
                $group->tasks()->save($task);

                // Tareas que no se borraron
                if ($requestTask['id']) {
                    $newTasks[] = $requestTask['id'];
                }
            }
            $deleteTasks = array_diff($oldTasks, $newTasks);
            foreach ($deleteTasks as $id) {
                Task::findOrFail($id)->delete();
            }

            // Redirects
            $redirects = Redirect::store($request->input('redirects'), $group);
            if ($redirects !== true) {
                throw $redirects;
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El grupo "' . $group->title . '" se ' . $msg . ' correctamente.',
                'data'    => $group,
            ];
        } catch (\Exception $e) {
            deleteFilesFromDb($group, $uploadedFiles);
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Elimina un grupo.
     *
     * @return array
     * @throws \Exception
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina los redirects
            $this->redirects()->delete();

            // Elimina el grupo
            $this->delete();

            // Elimina la imagen
            deleteFilesFromDb($this, $this->uploadFiles);

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El grupo "' . $this->title . '" se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
