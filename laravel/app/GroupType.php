<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupType extends Model
{
    /**
     * Grupos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
