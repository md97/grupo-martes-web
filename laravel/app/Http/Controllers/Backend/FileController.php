<?php

namespace App\Http\Controllers\Backend;

use App\File;
use App\FileType;
use App\Http\Requests\StoreFile;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Muestra el listado de archivos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // AJAX
        if (request()->expectsJson()) {
            return File::table();
        }

        $this->data['fileTypes'] = FileType::whereIn('name', ['Artículo', 'Libro'])->orderBy('name')->pluck('name', 'id');
        $this->data['title'] = 'Archivos | ' . config('app.name');

        return view('backend.files.index', $this->data);
    }

    /**
     * Devuelve la información para crear un nuevo archivo.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $file = new \stdClass();
        $file->downloads = [new \stdClass];
        $file->published = 1;

        // Tipos de archivo
        if ($request->input('type') == 'Traducción') {
            $this->data['fileTypes'] = FileType::whereIn('name', ['Texto prestado', 'Texto propio', 'Traducción'])->orderBy('name')->pluck('name', 'id');
        } else {
            $this->data['fileTypes'] = FileType::whereIn('name', ['Artículo', 'Libro'])->orderBy('name')->pluck('name', 'id');
        }
        $file->file_type_id = $this->data['fileTypes']->keys()->first();

        $this->data['file'] = $file;
        $this->data['type'] = $request->input('type');
        $this->data['route'] = route('admin.files.store');
        $this->data['method'] = 'post';

        return response()->json($this->data);
    }

    /**
     * Guarda un nuevo archivo.
     *
     * @param StoreFile $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreFile $request)
    {
        $response = File::store($request);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de archivos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => File::table(),
        ]);
    }

    /**
     * Devuelve la información para editar un archivo.
     *
     * @param File $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(File $file)
    {
        // Tipos de archivo
        if ($file->type->name == 'Texto prestado' || $file->type->name == 'Texto propio' || $file->type->name == 'Traducción') {
            $this->data['fileTypes'] = FileType::whereIn('name', ['Texto prestado', 'Texto propio', 'Traducción'])->orderBy('name')->pluck('name', 'id');
            $this->data['file'] = $file->load(['redirects' => function ($query) {
                $query->select(['redirectable_id', 'redirectable_type', 'old_url'])->orderBy('old_url');
            }]);
        } else {
            $this->data['fileTypes'] = FileType::whereIn('name', ['Artículo', 'Libro'])->orderBy('name')->pluck('name', 'id');
            $this->data['file'] = $file->load(['downloads.redirects' => function ($query) {
                $query->select(['redirectable_id', 'redirectable_type', 'old_url'])->orderBy('old_url');
            }]);
        }

        $this->data['type'] = $file->type->name;
        $this->data['route'] = route('admin.files.update', $file);
        $this->data['method'] = 'patch';
        $this->data['title'] = $file->title;

        return response()->json($this->data);
    }

    /**
     * Actualiza un archivo.
     *
     * @param StoreFile $request
     * @param File      $file
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(StoreFile $request, File $file)
    {
        $response = File::store($request, $file);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de archivos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => File::table(),
        ]);
    }

    /**
     * Elimina un archivo.
     *
     * @param File $file
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(File $file)
    {
        $response = $file->eliminar();

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de archivos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => File::table(),
        ]);
    }
}
