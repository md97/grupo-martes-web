<?php

namespace App\Http\Controllers\Backend;

use App\File;
use App\Group;
use App\GroupType;
use App\Http\Requests\StoreGroup;

class GroupController extends Controller
{
    /**
     * Muestra el listado de grupos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // AJAX
        if (request()->expectsJson()) {
            return Group::table();
        }

        $this->data['title'] = 'Grupos | ' . config('app.name');

        return view('backend.groups.index', $this->data);
    }

    /**
     * Devuelve la información para crear un nuevo grupo.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->data['groupTypes'] = GroupType::orderBy('name')->get(['id', 'name']);
        $this->data['group'] = json_decode('{"actual": 0, "published": 1, "group_type_id": ' . $this->data['groupTypes']->first()->id . ', "tasks": []}');

        // Archivos principales
        $this->data['mainFiles'] = File::orderBy('slug')->get()->transform(function ($item) {
            return ['id' => $item->id, 'fullTitle' => $item->fullTitle];
        });
        $this->data['filteredMainFiles'] = $this->data['mainFiles'];
        $this->data['selectedMainFiles'] = null;

        // Archivos secundarios
        $this->data['secondaryFiles'] = $this->data['mainFiles'];
        $this->data['filteredSecondaryFiles'] = $this->data['secondaryFiles'];
        $this->data['selectedSecondaryFiles'] = null;

        $this->data['route'] = route('admin.groups.store');
        $this->data['method'] = 'post';
        $this->data['title'] = 'Nuevo grupo';

        return response()->json($this->data);
    }

    /**
     * Guarda un nuevo grupo.
     *
     * @param StoreGroup $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreGroup $request)
    {
        $response = Group::store($request);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de grupos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => Group::table(),
        ]);
    }

    /**
     * Devuelve la información para editar un grupo.
     *
     * @param Group $group
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Group $group)
    {
        $this->data['groupTypes'] = GroupType::orderBy('name')->get(['id', 'name']);
        $this->data['group'] = $group->load(['tasks' => function ($query) {
            $query->orderBy('date');
        }, 'redirects' => function ($query) {
            $query->select(['redirectable_id', 'redirectable_type', 'old_url'])->orderBy('old_url');
        }]);

        // Archivos
        $files = File::orderBy('slug')->get();
        $selectedMainFiles = $group->files->where('pivot.reading', 1);
        $selectedSecondaryFiles = $group->files->where('pivot.reading', 0);
        $mainFiles = $files->diff($selectedSecondaryFiles);
        $secondaryFiles = $files->diff($selectedMainFiles);

        // Archivos principales
        $this->data['mainFiles'] = $mainFiles->sortBy('slug')->transform(function ($item) {
            return ['id' => $item->id, 'fullTitle' => $item->fullTitle];
        })->values();
        $this->data['filteredMainFiles'] = $this->data['mainFiles'];
        $this->data['selectedMainFiles'] = $selectedMainFiles->sortBy('slug')->transform(function ($item) {
            return ['id' => $item->id, 'fullTitle' => $item->fullTitle];
        })->values();

        // Archivos secundarios
        $this->data['secondaryFiles'] = $secondaryFiles->sortBy('slug')->transform(function ($item) {
            return ['id' => $item->id, 'fullTitle' => $item->fullTitle];
        })->values();
        $this->data['filteredSecondaryFiles'] = $this->data['secondaryFiles'];
        $this->data['selectedSecondaryFiles'] = $selectedSecondaryFiles->sortBy('slug')->transform(function ($item) {
            return ['id' => $item->id, 'fullTitle' => $item->fullTitle];
        })->values();

        $this->data['route'] = route('admin.groups.update', $group);
        $this->data['method'] = 'patch';
        $this->data['title'] = $group->title;

        return response()->json($this->data);
    }

    /**
     * Actualiza un grupo.
     *
     * @param StoreGroup $request
     * @param Group      $group
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(StoreGroup $request, Group $group)
    {
        $response = Group::store($request, $group);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de grupos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => Group::table(),
        ]);
    }

    /**
     * Elimina un grupo.
     *
     * @param Group $group
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $response = $group->eliminar();

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve la tabla de grupos para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => Group::table(),
        ]);
    }
}
