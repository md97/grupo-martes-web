<?php

namespace App\Http\Controllers;

use App\Mail\ContactoEnviado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function index()
    {
        return view('contacto.index');
    }

    public function send(Request $request)
    {
        Mail::send(new ContactoEnviado($request));
        return view('contacto.index');
    }
}