<?php

namespace App\Http\Controllers;

use App\Download;
use Storage;

class DownloadController extends Controller
{
    /**
     * Descarga uno de los archivos de un File.
     *
     * @param Download $download
     * @param          $name
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function show(Download $download, $name)
    {
        // Si el usuario no está logueado
        if (auth()->guest()) {
            // Si es un libro no lo puede descargar
            if ($download->file->type->name == 'Libro') {
                abort(404);
            }

            // Si no está publicado no lo puede descargar
            if (! $download->file->published) {
                abort(404);
            }
        }

        $path = Storage::path($download->name);
        $type = Storage::mimeType($download->name);
        $headers = ['Content-Type', $type];

        return response()->file($path, $headers);
    }
}
