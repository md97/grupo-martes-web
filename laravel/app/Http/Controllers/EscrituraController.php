<?php

namespace App\Http\Controllers;

use App\Escritura;
use Illuminate\Http\Request;

class EscrituraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // AJAX
        if (request()->expectsJson()) {
            $input = request()->all();
            if (request()->input("type") == "all"){
                return response()->json(Escritura::all());
            }
            if (array_key_exists("tag", $input)){
                $tag = request()->input("tag");
                $escritos = Escritura::where('tags', 'LIKE', "%{$tag}%")->get();
                $escrito_elegido = $escritos->random();
                return response()->json($escrito_elegido);
            }
            $escrito = Escritura::inRandomOrder()->first();
            return response()->json($escrito);
        }
        return view('escritura.index', $this->data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('escritura.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $escrito = new Escritura;
        $escrito->padres = $request->padres;
        $escrito->hijos = is_null($request->hijos) ? [] : $request->hijos;
        $escrito->tags = $request->tags;
        $escrito->texto = $request->texto;
        
        $escrito->save();

        return response()->json(["status"=>"success", "message"=> "El texto se guardo correctamente", "texto"=>$request->texto]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Escritura  $escritura
     * @param int $escritura
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($escritura)
    {
        $escrito = Escritura::find($escritura);
        return response()->json($escrito);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Escritura  $escritura
     * @return \Illuminate\Http\Response
     */
    public function edit(Escritura $escritura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Escritura  $escritura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Escritura $escritura)
    {
        $new_escritura = Escritura::find($escritura->id);

        $new_escritura->padres = $request->padres;
        $new_escritura->hijos = is_null($request->hijos) ? [] : $request->hijos;
        $new_escritura->tags = $request->tags;
        $new_escritura->texto = $request->texto;
        $new_escritura->save();
        
        return response()->json(["status"=>"success", "message"=> "El texto se edito correctamente"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Escritura  $escritura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Escritura $escritura)
    {
        Escritura::destroy($escritura->id);
        return response()->json(["status"=>"success", "message"=> "El texto se borro correctamente"]);
    }
}
