<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Muestra el listado de archivos.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Tipos de archivos
        $routeName = $request->route()->getName();
        switch ($routeName) {
            case 'files.textos-propios':
                $type = 'Texto propio';
                $title = 'Textos propios';
                break;

            case 'files.textos-prestados':
                $type = ['Texto prestado', 'Artículo'];
                $title = 'Textos prestados';
                break;

            case 'files.traducciones':
                $type = 'Traducción';
                $title = 'Traducciones';
                break;

            case 'files.libros':
                $type = 'Libro';
                $title = 'Libros';
                break;

            default:
                abort(404);
        }

        // Textos
        $files = File::whereHas('type', function ($query) use ($type) {
            if (is_array($type)) {
                foreach ($type as $i => $tipo) {
                    if ($i) {
                        $query->orWhere('name', $tipo);
                    } else {
                        $query->where('name', $tipo);
                    }
                }
            } else {
                $query->where('name', $type);
            }
        })->with(['type', 'downloads' => function ($query) {
            $query->orderBy('original_name');
        }])->orderBy('slug');

        // Si el usuario no está logueado, solo ve los archivos publicados
        if (auth()->guest()) {
            $files = $files->published();
        }

        $this->data['files'] = $files->get(['files.id', 'files.file_type_id', 'files.title', 'files.slug', 'files.author', 'files.published']);
        $this->data['class'] = 'files-index';
        $this->data['pageTitle'] = $title;
        $this->data['title'] = $title . ' | ' . config('app.name');

        return view('files.index', $this->data);
    }

    /**
     * Muestra un File (texto).
     *
     * @param File $file
     * @param      $slug
     *
     * @return void
     */
    public function show(File $file, $slug)
    {
        // Si el usuario no está logueado
        if (auth()->guest()) {
            // Si no está publicado no lo puede ver
            if (! $file->published) {
                abort(404);
            }
        }

        // Si el slug indicado NO es el correcto, redirecciona
        if ($file->slug != $slug) {
            return redirect(route('files.show', [$file, $file->slug]));
        }

        $this->data['file'] = $file;
        $this->data['class'] = 'files-show';
        $this->data['title'] = $file->fullTitle . ' | ' . config('app.name');

        return view('files.show', $this->data);
    }
}
