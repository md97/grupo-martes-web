<?php

namespace App\Http\Controllers;

use App\Group;

class GroupController extends Controller
{
    /**
     * Muestra el listado de grupos.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $groups = Group::orderByDesc('year')->orderBy('title');

        // Si el usuario no está logueado, solo ve los grupos publicados
        if (auth()->guest()) {
            $groups = $groups->published();
        }

        $this->data['groupsByYear'] = $groups->get()->groupBy('year');
        $this->data['class'] = 'groups-index';
        $this->data['title'] = 'Grupos en el pasado | ' . config('app.name');

        return view('groups.index', $this->data);
    }

    /**
     * Muestra un grupo.
     *
     * @param Group $group
     * @param       $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show(Group $group, $slug)
    {
        $files = $group->files()->with(['type', 'downloads' => function ($query) {
            $query->orderBy('original_name');
        }])->orderBy('slug');

        // Si el usuario no está logueado
        if (auth()->guest()) {
            // Si no está publicado no lo puede ver
            if (! $group->published) {
                abort(404);
            }

            // Solo ve los archivos publicados
            $files = $files->published();
        } else {
            // Tareas
            $group->load(['tasks' => function ($query) {
                $query->orderBy('date');
            }]);
        }

        // Si el slug indicado NO es el correcto, redirecciona
        if ($group->slug != $slug) {
            return redirect(route('groups.show', [$group, $group->slug]));
        }

        $this->data['group'] = $group;
        $this->data['files'] = $files->get(['files.id', 'files.file_type_id', 'files.title', 'files.slug', 'files.author', 'files.published']);
        $this->data['class'] = 'groups-show';
        $this->data['title'] = $group->title . ' | ' . config('app.name');

        return view('groups.show', $this->data);
    }
}
