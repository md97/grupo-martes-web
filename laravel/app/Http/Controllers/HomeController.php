<?php

namespace App\Http\Controllers;

use App\Group;

class HomeController extends Controller
{
    /**
     * Página principal del sitio.
     */
    public function index()
    {
        $this->data['currentGroups'] = Group::current()->orderBy('title');

        // Si el usuario no está logueado, solo ve los grupos publicados
        if (auth()->guest()) {
            $this->data['currentGroups'] = $this->data['currentGroups']->published();
        }

        $this->data['currentGroups'] = $this->data['currentGroups']->get(['id', 'title', 'slug', 'schedule', 'place', 'image']);
        $this->data['class'] = 'index-show';

        return view('index', $this->data);
    }
}
