<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Si no está logueado o no es admin
        if (auth()->guest() || ! auth()->user()->is_admin) {
            return redirect(route('index'));
        }

        return $next($request);
    }
}
