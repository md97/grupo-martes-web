<?php

namespace App\Http\Requests;

use App\FileType;
use Illuminate\Foundation\Http\FormRequest;

class StoreFile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->input('actionType')) {
            // Importación de archivos o libros a partir de links
            case 'importLinks':
                $rules = [
                    'files'                => 'required|array',
                    'files.*.file_type_id' => 'required|exists:file_types,id',
                    'files.*.title'        => 'required',
                    'files.*.url'          => 'required|url|distinct',
                    'files.*.published'    => 'required|boolean',
                ];
                break;

            // Importación de archivos o creación/edición de un texto
            default:
                $fileType = FileType::findOrFail($this->input('file_type_id'));

                $rules = [
                    'file_type_id' => 'required|exists:file_types,id',
                    'title'        => 'required',
                    'published'    => 'required|boolean',
                ];

                if ($fileType->name == 'Libro') {
                    $rules['downloads'] = 'required|array';
                    $rules['downloads.*.name'] = 'required_without:downloads.*.url|nullable|empty_if:downloads.*.url';
                    $rules['downloads.*.url'] = 'required_without:downloads.*.name|nullable|url|distinct';
                } elseif ($fileType->name == 'Artículo') {
                    $rules['downloads'] = 'required|array|max:1';
                }
        }

        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [
            'title'     => 'título',
            'published' => 'publicado',
            'files'     => 'archivos',
        ];

        // Archivos
        foreach ($this->input('downloads', []) as $i => $download) {
            $attributes['downloads.' . $i . '.name'] = 'archivo ' . ($i + 1);
            $attributes['downloads.' . $i . '.url'] = 'url ' . ($i + 1);
        }

        return $attributes;
    }
}
