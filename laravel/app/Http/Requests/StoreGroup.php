<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_type_id'       => 'required|exists:group_types,id',
            'title'               => 'required',
            'published'           => 'required|boolean',
            'current'             => 'required|boolean',
            'year'                => 'nullable|digits:4',
            'image'               => 'max:600',
            'mainFiles'           => 'nullable|array',
            'mainFiles.*'         => 'distinct|exists:files,id',
            'secondaryFiles'      => 'nullable|array',
            'secondaryFiles.*'    => 'distinct|exists:files,id',
            'tasks'               => 'array',
            'tasks.*.id'          => 'nullable|exists:tasks,id',
            'tasks.*.description' => 'required',
            'tasks.*.dateDmy'     => 'required|date_format:d/m/Y',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [
            'group_type_id'  => 'tipo de grupo',
            'title'          => 'título',
            'current'        => 'actual',
            'published'      => 'publicado',
            'year'           => 'año',
            'image'          => 'imagen',
            'mainFiles'      => 'archivos',
            'secondaryFiles' => 'archivos',
        ];

        // Textos principales
        foreach ($this->input('mainFiles', []) as $i => $file) {
            $attributes['mainFiles.' . $i] = 'texto principal ' . ($i + 1);
        }

        // Textos secundarios
        foreach ($this->input('secondaryFiles', []) as $i => $file) {
            $attributes['secondaryFiles.' . $i] = 'texto secundario ' . ($i + 1);
        }

        // Tareas
        foreach ($this->input('tasks', []) as $i => $task) {
            $attributes['tasks.' . $i . '.id'] = 'ID de la tarea ' . ($i + 1);
            $attributes['tasks.' . $i . '.description'] = 'descripción de la tarea ' . ($i + 1);
            $attributes['tasks.' . $i . '.dateDmy'] = 'fecha de la tarea ' . ($i + 1);
        }

        return $attributes;
    }
}
