<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoEnviado extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Request
     */
    public $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $mensaje)
    {
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mensaje->email)->markdown('contacto.mail')->with([
            'contacto' => $this->mensaje->mensaje
        ]);;
    }
}
