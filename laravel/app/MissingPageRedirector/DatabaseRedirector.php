<?php

namespace App\MissingPageRedirector;

use App\Redirect;
use Symfony\Component\HttpFoundation\Request;
use Spatie\MissingPageRedirector\Redirector\Redirector;

class DatabaseRedirector implements Redirector
{
    public function getRedirectsFor(Request $request): array
    {
        return Redirect::with('redirectable')->get()->flatMap(function ($redirect) {
            return [$redirect->old_url => $redirect->new_url];
        })->toArray();
    }
}
