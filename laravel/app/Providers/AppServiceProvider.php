<?php

namespace App\Providers;

use Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Uno de los 2 campos tiene que estar vacío
        Validator::extendDependent('empty_if', function ($attribute, $value, $parameters, $validator) {
            $validator->addReplacer('empty_if', function ($message, $attribute, $rule, $parameters) use ($validator) {
                return str_replace(':other', $validator->customAttributes[$parameters[0]], $message);
            });

            if (Arr::get($validator->getData(), $parameters[0])) {
                return is_null($value);
            }

            return true;
        }, ':attribute tiene que estar vacío si :other no lo está.');
    }
}
