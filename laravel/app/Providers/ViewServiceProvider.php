<?php

namespace App\Providers;

use App\Group;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.default', 'layouts.error', 'layouts.escritura'], function ($view) {
            $navGroups = Group::select(['id', 'title', 'slug'])->current()->orderBy('title');

            // Si el usuario no está logueado, solo ve los grupos publicados
            if (auth()->guest()) {
                $navGroups = $navGroups->published();
            } else {
                // Se traen las tareas
                $navGroups = $navGroups->withCount(['tasks' => function ($query) {
                    $query->hasPending();
                }]);
            }

            $view->with('navGroups', $navGroups->get());
        });
    }
}
