<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    /**
     * Get the owning redirectable model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function redirectable()
    {
        return $this->morphTo();
    }

    /**
     * Devuelve la URL a la que hay que redireccionar.
     *
     * @param $value
     *
     * @return string|null
     */
    public function getNewUrlAttribute($value)
    {
        // Si tiene cargada una new_url
        if ($value) {
            return $value;
        }

        // Sino trae la URL del owner
        if ($this->redirectable) {
            return $this->redirectable->getShowUrl();
        }

        return null;
    }

    /**
     * Crea o actualiza redirects.
     *
     * @param $redirects
     * @param $owner
     *
     * @return bool|\Exception
     * @throws \Exception
     */
    public static function store($redirects, $owner)
    {
        DB::beginTransaction();
        try {
            // Borra los redirects eliminados
            $oldRedirects = $owner->redirects()->pluck('old_url')->toArray();
            $newRedirects = [];
            if ($redirects) {
                // Deja las URLs como relativas y les aplica un trim
                $newRedirects = array_filter(
                    array_map(function ($value) {
                        return mb_parse_url(rtrim(trim($value), '/'))['path'];
                    }, explode("\n", str_replace("\r", '', $redirects)))
                );
            }

            $deleteRedirects = array_diff($oldRedirects, $newRedirects);
            foreach ($deleteRedirects as $oldUrl) {
                self::firstWhere('old_url', $oldUrl)->delete();
            }

            // Carga los nuevos redirects
            $addRedirects = array_diff($newRedirects, $oldRedirects);
            foreach ($addRedirects as $oldUrl) {
                $redirect = new self;
                $redirect->old_url = $oldUrl;
                $owner->redirects()->save($redirect);
            }

            // Commit
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();

            return $e;
        }
    }
}
