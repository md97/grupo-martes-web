<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date'];


    /**
     * Accessors que se agregan al convertir el modelo a JSON (para el calendario).
     *
     * @var array
     */
    protected $appends = ['dateDmy'];

    /**
     * Grupo al que pertenece la tarea.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Devuelve la fecha formateada en d/m/Y.
     *
     * @return Carbon|null
     */
    public function getDateDmyAttribute()
    {
        if ($this->date) {
            return $this->date->format('d/m/Y');
        }

        return null;
    }

    /**
     * Guarda la fecha transformándola de d/m/Y a Y-m-d.
     *
     * @param $date
     */
    public function setDateDmyAttribute($date)
    {
        $this->attributes['date'] = ymd($date);
    }

    /**
     * Determina si hay tareas pendientes.
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasPending($query)
    {
        return $query->whereDate('date', '>=', now());
    }
}
