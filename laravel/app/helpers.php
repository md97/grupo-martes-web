<?php

use Carbon\Carbon;

if (! function_exists('uploadFiles')) {
    /**
     * Sube archivos y borra viejos.
     *
     * @param $obj        object
     * @param $fieldNames string|array
     * @param $request    Illuminate\Foundation\Http\FormRequest
     * @param $update     bool
     *
     * @return array
     * @throws Exception
     */
    function uploadFiles($obj, $fieldNames, $request, $update)
    {
        $fieldNames = is_array($fieldNames) ? $fieldNames : [$fieldNames];
        $uploadedFiles = [];

        foreach ($fieldNames as $fieldName) {
            // Archivo
            $delete = ! isset($_FILES[$fieldName]) ? true : false;
            $file = $request->file($fieldName);

            if ($file) {
                // Si ya hay un archivo cargado, hay que borrar el anterior
                $delete = $obj->$fieldName ?: false;

                // Si la extensión es JPEG la cambia a JPG para que sea de 3 letras
                $extension = strtolower($file->getClientOriginalExtension());
                $extension = ($extension == 'jpeg') ? 'jpg' : $extension;

                // Resta la cantidad de caracteres de la extensión al largo total del nombre
                $length = 38 - strlen($extension);

                // Chequea que el nombre no se repita
                do {
                    $nombre = Str::random($length) . '.' . $extension;
                } while (file_exists($obj->path . $nombre));

                // Sí el archivo se subió correctamente
                if ($file->move($obj->path, $nombre)) {
                    $obj->$fieldName = $nombre;
                    $uploadedFiles[] = $fieldName;
                } else {
                    $error = 'Se produjo un error al subir el archivo "' . $file->getClientOriginalName() . '".';
                    throw new \Exception($error);
                }
            } // Sí es un Update y se borró el archivo
            elseif ($update && $delete) {
                $delete = $obj->$fieldName;
                $obj->$fieldName = null;
            }

            // Elimina el archivo
            if ($delete) {
                deleteFile($delete, $obj->path);
            }
        }

        return $uploadedFiles;
    }
}

if (! function_exists('deleteFilesFromDb')) {
    /**
     * Borra archivos que corresponden a campos de la DB.
     *
     * @param $obj        object
     * @param $fieldNames string | array
     *
     * @throws Exception
     */
    function deleteFilesFromDb($obj, $fieldNames)
    {
        $fieldNames = is_array($fieldNames) ? $fieldNames : [$fieldNames];
        foreach ($fieldNames as $fieldName) {
            if ($obj->$fieldName) {
                deleteFile($obj->$fieldName, $obj->path);
            }
        }
    }
}

if (! function_exists('deleteFile')) {
    /**
     * Elimina archivos y previene directory traversal.
     *
     * @param $fileName string nombre del archivo
     * @param $path     string ruta del archivo
     *
     * @return bool
     * @throws Exception
     */
    function deleteFile($fileName, $path)
    {
        $file = $path . $fileName;
        if (file_exists($file)) {
            // Previene directory traversal
            if (dirname(realpath($file)) == realpath($path)) {
                // Elimina el archivo
                if (! @unlink($file)) {
                    $error = 'No se pudo eliminar el archivo "' . $fileName . '".';
                    throw new \Exception($error);
                }
            } else {
                $error = 'No se pudo eliminar el archivo "' . $fileName . '".';
                throw new \Exception($error);
            }
        }

        return true;
    }
}

if (! function_exists('strip_markdown')) {
    /**
     * Remueve el markdown y deja un texto plano.
     *
     * @param $string string
     *
     * @return string|null
     */
    function strip_markdown($string)
    {
        if ($string) {
            return strip_tags(Markdown::convertToHtml($string));
        }

        return null;
    }
}

if (! function_exists('ymd')) {
    /**
     * d/m/Y to Y-m-d para los mutators
     *
     * @param  $date  string
     *
     * @return null|Carbon
     */
    function ymd($date)
    {
        if ($date) {
            $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        } else {
            $date = null;
        }

        return $date;
    }
}

if (! function_Exists('mb_parse_url')) {
    /**
     * UTF-8 aware parse_url() replacement.
     *
     * @param $url
     *
     * @return array
     */
    function mb_parse_url($url)
    {
        $enc_url = preg_replace_callback(
            '%[^:/@?&=#]+%usD',
            function ($matches)
            {
                return urlencode($matches[0]);
            },
            $url
        );

        $parts = parse_url($enc_url);

        if ($parts === false)
        {
            throw new \InvalidArgumentException('Malformed URL: ' . $url);
        }

        foreach ($parts as $name => $value)
        {
            $parts[$name] = urldecode($value);
        }

        return $parts;
    }
}
