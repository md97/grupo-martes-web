<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('group_type_id');
            $table->string('title');
            $table->string('slug');
            $table->string('schedule')->nullable();
            $table->string('place')->nullable();
            $table->longText('description')->nullable();
            $table->longText('notes')->nullable();
            $table->year('year')->nullable();
            $table->char('image', 39)->nullable();
            $table->boolean('current')->default(0);
            $table->boolean('published')->default(1);

            $table->timestamps();

            $table->foreign('group_type_id')->references('id')->on('group_types')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
