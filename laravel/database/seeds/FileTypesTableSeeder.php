<?php

use App\FileType;
use Illuminate\Database\Seeder;

class FileTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileType::create(['name' => 'Artículo']);
        FileType::create(['name' => 'Libro']);
        FileType::create(['name' => 'Texto prestado']);
        FileType::create(['name' => 'Texto propio']);
        FileType::create(['name' => 'Traducción']);
    }
}
