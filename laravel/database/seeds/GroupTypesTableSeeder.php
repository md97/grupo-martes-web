<?php

use App\GroupType;
use Illuminate\Database\Seeder;

class GroupTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupType::create(['name' => 'Anual']);
        GroupType::create(['name' => 'Flotante']);
        GroupType::create(['name' => 'Proyecto']);
        GroupType::create(['name' => 'Verano']);
    }
}
