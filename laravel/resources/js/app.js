/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import SvgVue from 'svg-vue';
import Vue from 'vue';
import {ClientTable, Event} from 'vue-tables-2';
import MySortControl from './backend/components/MySortControl';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('groups-index', require('./backend/components/GroupsIndex.vue').default);
Vue.component('files-index', require('./backend/components/FilesIndex.vue').default);
Vue.component('escritura-index', require('./escritura/components/EscrituraIndex.vue').default);
Vue.component('escritura-create', require('./escritura/components/EscrituraCreate.vue').default);
Vue.use(SvgVue);
Vue.use(ClientTable, {}, false, 'bootstrap4', {sortControl: MySortControl});
Vue.filter('redirectUrls', function (redirect) {
    return _.map(redirect, 'old_url').join('\n');
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    mounted() {
        // autofocus Fx fix https://github.com/vuejs/vue/issues/8112
        const input = document.querySelector('[autofocus]');
        if (input) {
            input.focus();
        }
    }
});
