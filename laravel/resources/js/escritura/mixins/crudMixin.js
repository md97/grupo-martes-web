import EasyMDE from 'easymde';
import qs from "qs";
export const crudMixin = {
    data() {
        return {
            loading: true,          // Loading inicial de la página
            fetchedData: null,      // Contenido inicial de la página
            father: null,           // El texto superior
            son: null,              // El texto inferior
            holySpirit: null,        // El texto en el centro
            currentTags: null,
            allTexts: null,
            selectedFathers: [],
            selectedSons: [],
            currentInputText: "",
            editingTextId: null,
            isEditing: false,
            currentTagText: "",
            blackFilter: "invert(0%) sepia(7%) saturate(7438%) hue-rotate(60deg) brightness(111%) contrast(100%)",
            whiteFilter: "invert(100%) sepia(100%) saturate(0%) hue-rotate(288deg) brightness(102%) contrast(102%)",
            easyMDEconfigs: {
                status: false, // disable the status bar at the bottom
                spellChecker: false // disable spell check
            },
            scrollbarSettings: {
                suppressScrollY: true,
                suppressScrollX: false,
                wheelPropagation: false
            },
            currentEditingText: null
        }
    },
    created() {
        // Trae la información inicial
        this.fetchData(this.fetchUrl);
        this.fetchAllTags(this.fetchUrl);
        this.fetchAllTexts(this.fetchUrl);
        
    },
    computed: {
        easymde() {
            return this.$refs.markdownEditor.easymde;
        }
    },
    mounted() {
        
        console.log('Component mounted.')

        $(document).on("click", ".trash-bin-icon", function () {
            var myTextId = $(this).data('text');
            $(".modal-footer #delete-btn").val( myTextId );
       });
       $(document).on("click", ".pencil-icon", function () {
        var myTextId = $(this).data('text');
        $(".modal-footer #edit-btn").val( myTextId );
   });
    },
    methods: {
        shuffle(array) {
            let currentIndex = array.length,  randomIndex;
          
            // While there remain elements to shuffle.
            while (currentIndex != 0) {
          
              // Pick a remaining element.
              randomIndex = Math.floor(Math.random() * currentIndex);
              currentIndex--;
          
              // And swap it with the current element.
              [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
            }
          
            return array;
          },
        // Trae la data de la DB
        fetchData(url) {
            this.loading = true;
            axios.get(url).then((response) => {
                this.fetchedData = response.data;
                this.holySpirit = this.fetchedData;
                this.loading = false;
                return;
            }).then(()=>{
                let father_id = this.holySpirit.padres[Math.floor(Math.random() * this.holySpirit.padres.length)];
                axios.get(url + `/${father_id}`).then((response) => {
                this.father = response.data;
                return;
            });
            }).then(()=>{
                let son_id = this.holySpirit.hijos[Math.floor(Math.random() * this.holySpirit.hijos.length)];
                axios.get(url + `/${son_id}`).then((response) => {
                this.son = response.data;
                return;
            });
            });
            
        },

        moveUp(url) {
            let new_father_id = this.father.padres[Math.floor(Math.random() * this.father.padres.length)];
            axios.get(url + `/${new_father_id}`).then((response) => {
                this.son = this.holySpirit;
                this.holySpirit = this.father;
                this.father = response.data;
                return;
            });
        },

        moveDown(url){
            let new_son_id = this.son.hijos[Math.floor(Math.random() * this.son.hijos.length)];
            axios.get(url + `/${new_son_id}`).then((response) => {
                this.father = this.holySpirit;
                this.holySpirit = this.son;
                this.son = response.data;
                return;
            });
        },

        fetchAllTags(url){
            this.currentTags = new Set();
            axios.get(url + "?type=all").then((response) => {
                response.data.forEach((escrito) => {
                    for(let tag of escrito.tags){
                        this.currentTags.add(tag);
                    }
                });
                this.currentTags = this.shuffle(Array.from(this.currentTags));
            });
        },
        getByTag(event){
            const tag = event.target.innerText.trim();
            const url = this.fetchUrl
            axios.get(url + `?tag=${tag}`).then((response) => {
                this.fetchedData = response.data;
                this.holySpirit = this.fetchedData;
                this.loading = false;
                return;
            }).then(()=>{
                let father_id = this.holySpirit.padres[Math.floor(Math.random() * this.holySpirit.padres.length)];
                axios.get(url + `/${father_id}`).then((response) => {
                this.father = response.data;
                return;
            });
            }).then(()=>{
                let son_id = this.holySpirit.hijos[Math.floor(Math.random() * this.holySpirit.hijos.length)];
                axios.get(url + `/${son_id}`).then((response) => {
                this.son = response.data;
                return;
            });
            });
        },
        addInputTag(event){
            const tag = event.target.innerText.trim();
            if (this.currentTagText.trim().length > 0){
                this.currentTagText += `, ${tag}`;
            } else {
                this.currentTagText = tag;
            }
        },
        fetchAllTexts(url){
            axios.get(url + "?type=all").then((response) => {
                this.allTexts = response.data;
            });
        },
        toggleTextBoxColor(event, type, color) {
            let target = event.target;
            if(event.target.className === "card-text"){ // Por si hace click en el text
                target = event.target.parentElement;
            }
            if(event.target.className === `col-sm-12 col-lg-6 ${type}`){ // Por si hace click afuera
                target = event.target.querySelector(".card-body");
            }
            target.style.backgroundColor = color;
            target.style.color = color === "black" ? "white": "black";
            // toggle svg colors
            target.querySelector(".trash-bin-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
            target.querySelector(".pencil-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
        },
        toggleText(type, text, event) {
            text = String(text);
            if(typeof event.target.className.baseVal !== 'string' || !event.target.className.includes('icon')) {
                const selectedTextsByType = type === "father" ? this.selectedFathers : this.selectedSons;
                if(selectedTextsByType.indexOf(text) > -1){
                    const index = selectedTextsByType.indexOf(text);
                    if (index > -1) {
                        selectedTextsByType.splice(index, 1); // 2nd parameter means remove one item only
                    }
                    this.toggleTextBoxColor(event, type, "white");
                } else{
                    console.log(text);
                    selectedTextsByType.push(text);
                    console.log(this.selectedFathers);
                    this.toggleTextBoxColor(event, type, "black");
                }
        }
        },
        submitText(form){
            let tags = this.currentTagText.split(",").map(element => {
                return element.trim();
              });
            let padres = this.selectedFathers;
            let hijos = this.selectedSons;
            const params = new URLSearchParams();
            params.append('padres', padres);
            params.append('hijos', hijos);
            params.append('tags', tags);
            params.append("texto", this.currentInputText);
            const data = {
                "padres": padres,
                "hijos": hijos,
                "tags": tags,
                "texto": this.currentInputText
            }
            if(this.currentEditingText){
                axios.put(this.fetchUrl  + `/${this.currentEditingText.id}`, qs.stringify(data))
                .then((response) => {
                    console.log(response.data);
                });
                this.currentEditingText = null;
            } else {
                axios.post(this.fetchUrl, qs.stringify(data))
                .then((response) => {
                    console.log(response.data);
                });
            }
            
            document.location.reload();
        },
        deleteText(event){
            const url = this.fetchUrl;
            axios.delete(url + `/${event.target.value}`).then((response) => {
                console.log(response);
            });
            document.location.reload();
        },
        editText(event){
            document.querySelectorAll(".close").forEach(element => element.click());
            this.currentEditingText = this.allTexts.find(text => text.id === Number(event.target.value));
            this.currentInputText = this.currentEditingText.texto;
            this.currentTagText = this.currentEditingText.tags.join(", ");
            // Seleccionar los padres e hijos correspondientes
            this.selectedFathers = this.currentEditingText.padres;
            this.selectedSons = this.currentEditingText.hijos;
            this.selectedFathers.forEach(element => {
                let color = "black";
                let searchFatherById = `#father-${element}`;
                let target = document.querySelector(searchFatherById);
                target.style.backgroundColor = color;
                target.style.color = color === "black" ? "white": "black";
                // toggle svg colors
                target.querySelector(".trash-bin-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
                target.querySelector(".pencil-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
            });
            this.selectedSons.forEach(element => {
                let color = "black";
                let searchSonById = `#son-${element}`;
                let target = document.querySelector(searchSonById);
                target.style.backgroundColor = color;
                target.style.color = color === "black" ? "white": "black";
                // toggle svg colors
                target.querySelector(".trash-bin-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
                target.querySelector(".pencil-icon").style.filter = color === "black" ?  this.whiteFilter : this.blackFilter;
            });
        }
    }
}
