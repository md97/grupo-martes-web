@extends('backend.layouts.app')

@section('content')
    <files-index
        fetch-url="{{ route('admin.files.index') }}"
        create-url="{{ route('admin.files.create') }}"
        :file-types="{{ $fileTypes }}"
        edit="{{ request()->input('edit', null) }}"
    ></files-index>
@endsection
