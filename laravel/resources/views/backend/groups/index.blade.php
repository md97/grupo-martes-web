@extends('backend.layouts.app')

@section('content')
    <groups-index
        fetch-url="{{ route('admin.groups.index') }}"
        create-url="{{ route('admin.groups.create') }}"
        edit="{{ request()->input('edit', null) }}"
    ></groups-index>
@endsection
