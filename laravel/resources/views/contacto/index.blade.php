@extends('layouts.default')

@section('content')
<h2 class="h6 font-weight-bold mb-3">Contacto</h2>

<div class="contact-grid">
    <p>Si querés descargarte algún libro de la página, leer los apuntes, sumarte a algún grupo o revisar nuestra página de escritura, escribinos! Esperá la respuesta y vemos qué se puede hacer. </p>
</div>

<div class="contacto-container">
  <form action="{{ route('contacto.enviar') }}" method="post">
    @csrf
    <label for="fname">Nombre</label>
    <input type="text" class="input-contacto" id="fname" name="firstname" placeholder="Nombre">

    <label for="email">E-mail</label>
    <input type="text" class="input-contacto" id="email" name="email" placeholder="E-mail">
    

    <label for="mensaje">Mensaje</label>
    <textarea id="mensaje" name="mensaje" placeholder="Mensaje" style="height:200px"></textarea>

    <input id="contacto-enviar" type="submit" value="Enviar">

  </form>
</div>
@endsection
