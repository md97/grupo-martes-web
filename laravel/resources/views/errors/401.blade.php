@extends('layouts.error', ['title' => __('Unauthorized') . ' | ' . config('app.name')])

@section('code', '401')
@section('message', __('Unauthorized'))
