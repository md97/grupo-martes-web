@extends('layouts.error', ['title' => __('Forbidden') . ' | ' . config('app.name')])

@section('code', '403')
@section('message', __($exception->getMessage() ?: 'Forbidden'))
