@extends('layouts.error', ['title' => __('Not Found') . ' | ' . config('app.name')])

@section('code', '404')
@section('message', 'Por remodelación puede que lo que estás buscando esté hoy en otro lugar... pero seguremente está por algún rincón de nuestra nueva página... Te invitamos a seguir buscando!')
