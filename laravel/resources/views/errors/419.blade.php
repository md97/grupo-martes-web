@extends('layouts.error', ['title' => __('Page Expired') . ' | ' . config('app.name')])

@section('code', '419')
@section('message', __('Page Expired'))
