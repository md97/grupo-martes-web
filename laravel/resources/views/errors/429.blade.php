@extends('layouts.error', ['title' => __('Too Many Requests') . ' | ' . config('app.name')])

@section('code', '429')
@section('message', __('Too Many Requests'))
