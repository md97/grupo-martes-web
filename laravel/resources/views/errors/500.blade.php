@extends('layouts.error', ['title' => __('Server Error') . ' | ' . config('app.name')])

@section('code', '500')
@section('message', __('Server Error'))
