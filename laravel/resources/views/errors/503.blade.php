@extends('layouts.error', ['title' => __('Service Unavailable') . ' | ' . config('app.name')])

@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Service Unavailable'))
