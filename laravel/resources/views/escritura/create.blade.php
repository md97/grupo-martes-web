@extends('layouts.escritura')

@section('content')
    <escritura-create
        fetch-url="{{ route('escritura.index') }}"
    ></escritura-create>
@endsection
