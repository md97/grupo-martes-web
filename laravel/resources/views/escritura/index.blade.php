@extends('layouts.escritura')

@section('content')
    <escritura-index
        fetch-url="{{ route('escritura.index') }}"
    ></escritura-index>
@endsection
