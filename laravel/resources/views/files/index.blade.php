@extends('layouts.default')

@section('content')
    <h1 class="page-title text-center">{{ $pageTitle }}</h1>
    @if ($files->count())
        <ul class="list-unstyled mt-4">
            @foreach ($files as $file)
                <li>
                    @if (! $file->published)
                        <i class="fas fa-exclamation-circle text-warning" title="No publicado"></i>
                    @endif
                    @if ($file->downloads->count())
                        @if ($file->type->name == 'Libro' && auth()->guest())
                            <span>{{ $file->fullTitle }}</span> (<a href="{{ route('contacto.index') }}">solicitar</a>)
                        @elseif ($file->downloads->count() > 1)
                            <span>{{ $file->fullTitle }}</span>
                            @foreach ($file->downloads as $download)
                                {{ $loop->first ? '(' : '' }}<a href="{{ route('downloads.show', [$download, $download->original_name]) }}">{{ substr($download->name, strpos($download->name, '.') + 1) }}</a>{{ $loop->remaining ? ', ' : '' }}{{ $loop->last ? ')' : '' }}
                            @endforeach
                        @else
                            <a href="{{ route('downloads.show', [$file->downloads->first(), $file->downloads->first()->original_name]) }}" target="_blank">{{ $file->fullTitle }}</a>
                        @endif
                    @else
                        <a href="{{ route('files.show', [$file, $file->slug]) }}" target="_blank">{{ $file->fullTitle }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
    @endif
@endsection
