@extends('layouts.default')

@section('content')
    @if (! $file->published)
        <div class="alert alert-warning">Éste texto no está publicado, solo lo pueden ver los usuarios logueados.</div>
    @endif

    <h1 class="page-title text-center">{{ $file->fullTitle }}</h1>

    @if (auth()->check() && auth()->user()->is_admin)
        <a class="btn btn-sm btn-outline-secondary mb-3" href="{{ route('admin.files.index', ['edit' => $file]) }}" target="_blank">Editar</a>
    @endif

    <div class="markdown text-justify">
        @markdown($file->text)
    </div>
@endsection
