@extends('layouts.default')

@section('content')
    <h1 class="page-title text-center">Grupos en el pasado</h1>
    <p class="text-justify">Estos grupos son grupos que tuvieron su recorrido pero que en algún momento se detuvieron o bien porque encontraron un nuevo derrotero que los llevo a otra parte, o bien porque cumplieron a grandes rasgos su plan, bien porque se agotaron y tuvieron que dejar paso a otra cosa. Esperan, claro, la oportunidad para volver… son el recuerdo presente de los otros grupos.</p>
    @if ($groupsByYear->count())
        <ul class="list-unstyled mt-4">
            @foreach ($groupsByYear as $year => $groups)
                @foreach ($groups as $group)
                    <li {!! $loop->first ? ' class="mt-3" id="año-' . $year . '"' : '' !!}>
                        @if (! $group->published)
                            <i class="fas fa-exclamation-circle text-warning" title="No publicado"></i>
                        @endif
                       <a href="{{ route('groups.show', [$group, $group->slug]) }}">{{ $group->fullTitle }}</a>
                    </li>
                @endforeach
            @endforeach
        </ul>
    @endif
@endsection
