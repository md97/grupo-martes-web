@extends('layouts.default')

@section('content')
    @if (! $group->published)
        <div class="alert alert-warning">Éste grupo no está publicado, solo lo pueden ver los usuarios logueados.</div>
    @endif

    <h1 class="page-title group-title text-center">{{ $group->year ? $group->year . ' - ' . $group->title : $group->title }}</h1>

    @if ($group->image)
        <img class="group-img img-fluid mx-auto d-block" src="{{ $group->imageUrl }}" alt="{{ $group->title }}">
    @endif

    @if ($group->schedule || $group->place || $files->count())
        <div class="group-info text-center text-muted pt-4">
            @if ($group->schedule)
                <span class="group-info-item">
                    <i class="fal fa-calendar-alt"></i>
                    {{ $group->schedule }}
                </span>
            @endif
            @if ($group->place)
                <span class="group-info-item">
                    <i class="fal fa-map-marker-alt"></i>
                    {{ $group->place }}
                </span>
            @endif
            @if ($files->count())
                <a class="group-info-item text-muted" href="#textos">
                    <i class="fal fa-books"></i>
                    Textos
                </a>
            @endif
        </div>
    @endif

    @auth
        @if (auth()->user()->is_admin)
            <a class="btn btn-sm btn-outline-secondary mb-4" href="{{ route('admin.groups.index', ['edit' => $group]) }}" target="_blank">Editar</a>
        @endif

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" data-toggle="tab" href="#description" role="tab" aria-selected="true">Descripción</a>
            </li>
            @if ($group->notes)
                <li class="nav-item" role="presentation">
                    <a class="nav-link" data-toggle="tab" href="#notes" role="tab" aria-selected="false">Apuntes</a>
                </li>
            @endif
            @if ($group->tasks->count())
                <li class="nav-item" role="presentation">
                    <a class="nav-link" data-toggle="tab" href="#tasks" role="tab" aria-selected="false">Tareas</a>
                </li>
            @endif
        </ul>
    @endauth

    <div class="tab-content">
        <div class="tab-pane fade show active" id="description" role="tabpanel">
            <div class="markdown group-description text-justify pt-4">
                @markdown($group->description)
            </div>
        </div>
        @auth
            @if ($group->notes)
                <div class="tab-pane fade" id="notes" role="tabpanel">
                    <div class="markdown group-description text-justify pt-4">
                        @markdown($group->notes)
                    </div>
                </div>
            @endif
            @if ($group->tasks->count())
                <div class="tab-pane fade" id="tasks" role="tabpanel">
                    <div class="markdown group-description text-justify pt-4">
                        <ul class="list-unstyled">
                            @foreach ($group->tasks as $task)
                                <li>{{ $task->dateDmy }} - {{ $task->description }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        @endauth
    </div>

    @if ($files->count())
        <hr>
        <ul class="group-files list list-unstyled" id="textos">
            @foreach ($files as $file)
                <li class="groupo-files-item">
                    @if (! $file->published)
                        <i class="fas fa-exclamation-circle text-warning" title="No publicado"></i>
                    @endif
                    @if ($file->downloads->count())
                        @if ($file->type->name == 'Libro' && auth()->guest())
                            <span>{{ $file->fullTitle }}</span> (<a href="{{ route('contacto.index') }}">solicitar</a>)
                        @elseif ($file->downloads->count() > 1)
                            <span>{{ $file->fullTitle }}</span>
                            @foreach ($file->downloads as $download)
                                {{ $loop->first ? '(' : '' }}<a href="{{ route('downloads.show', [$download, $download->original_name]) }}">{{ substr($download->name, strpos($download->name, '.') + 1) }}</a>{{ $loop->remaining ? ', ' : '' }}{{ $loop->last ? ')' : '' }}
                            @endforeach
                        @else
                            <a href="{{ route('downloads.show', [$file->downloads->first(), $file->downloads->first()->original_name]) }}" target="_blank">{{ $file->fullTitle }}</a>
                        @endif
                    @else
                        <a href="{{ route('files.show', [$file, $file->slug]) }}" target="_blank">{{ $file->fullTitle }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
    @endif
@endsection
