<nav class="main-navbar navbar navbar-expand-md navbar-light shadow-sm fixed-top">
    <div class="container">
        <a class="navbar-brand {{ Ekko::isActiveRoute('index') }}" href="{{ route('index') }}">
            {{ config('app.name', 'Grupo Martes') }}
        </a>

        <ul class="navbar-nav d-md-none mr-auto ml-2 navbar-current-groups">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actuales</a>
                <div class="dropdown-menu shadow">
                    @foreach ($navGroups as $group)
                        <a class="dropdown-item {{ Ekko::isActive(route('groups.show', [$group, $group->slug], false)) }}" href="{{ route('groups.show', [$group, $group->slug]) }}">
                            {{ $group->title }}
                            @if (auth()->check() && $group->research)
                                <span class="has-research"></span>
                            @endif
                        </a>
                    @endforeach
                </div>
            </li>
        </ul>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-label="Abrir menú">
            <i class="toggler-icon fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item {{ Ekko::isActiveRoute('groups.*') }}">
                    <a class="nav-link" href="{{ route('groups.index') }}">Grupos</a>
                </li>
                @guest
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('escritura.index') }}">Lectura</a>
                </li>
                @else
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('escritura.index') }}">Lectura</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('escritura.create') }}">Escritura</a>
                </li>
                @endguest
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('contacto.index') }}">Contacto</a>
                </li>
                <li class="nav-item dropdown ml-md-4 {{ Ekko::isActiveRoute('files.*') }}">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Textos</a>
                    <div class="dropdown-menu shadow">
                        <a class="dropdown-item" href="{{ route('files.textos-prestados') }}">Textos prestados</a>
                        <a class="dropdown-item" href="{{ route('files.textos-propios') }}">Textos propios</a>
                        <a class="dropdown-item" href="{{ route('files.traducciones') }}">Traducciones</a>
                        <a class="dropdown-item" href="{{ route('files.libros') }}">Libros</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link nav-link-login" href="{{ route('login') }}">Login</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow">
                            @if (auth()->user()->is_admin)
                                <a class="dropdown-item" href="{{ route('admin.home') }}">Panel</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
