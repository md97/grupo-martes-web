<div class="card shadow-sm">
    <div class="card-header">
        <span>Grupos actuales</span>
    </div>
    <div class="list-group list-group-flush">
        @foreach ($navGroups as $group)
            <a class="list-group-item list-group-item-action {{ Ekko::isActive(route('groups.show', [$group, $group->slug], false)) }}" href="{{ route('groups.show', [$group, $group->slug]) }}">
                {{ $group->title }}
                @if (auth()->check() && $group->tasks_count)
                    <span class="has-task"></span>
                @endif
            </a>
        @endforeach
    </div>
</div>
