@extends('layouts.default')

@push('scripts')
    <script>
        $(function () {
            $('.index-article').click(function () {
                var $this = $(this);
                var $modal = $('#articleModal');
                var title = $this.find('.card-title').text();
                var body = $this.find('.article-text').html();

                $modal.find('.modal-title').text(title);
                $modal.find('.modal-body').html(body);
                $modal.modal('show');
            });
        });
    </script>
@endpush

@section('content')
    <section>
        <h2 class="h6 font-weight-bold mb-3">Componiendo grupos</h2>

        <div id="articleModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

        <div class="articles-grid row mx-n2" data-columns>
            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/los-grupos-crecen-por-todas-partes.jpg')) }}" class="card-img img-fluid" width="280" height="516" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">Los grupos crecen por todas partes</h2>
                    <div id="articleText1" class="article-text d-none">
                        <p class="text-justify">Los grupos crecen y nos crecen por todas partes. Basta que nuestra errancia se haga más lenta, se demore o encuentre otras errancias casualmente al mismo destiempo para que un grupo crezca o nos crezca.<br>Pero los grupos se sostienen de un modo totalmente distinto a como crecen. Pueden crecernos grupos por todas partes sin que ninguno llegue a ser sostenido. Y aún así los grupos que se sostienen no dejan de crecer ni de crecernos.<br>Para sostener un grupo, aparte de dejarlo crecer, hace falta encontrarle una memoria. La memoria de un grupo son memorias (y no podría ser de otro modo). Un grupo tiene tantas memorias y sin embargo solo hace falta que una de esas memorias se haga presente para que el grupo comience a sostenerse, precariamente, apenas agarrado de esa memoria como de un madero en el tiempo.<br>Pero eso ya es mucho, porque basta con una memoria en la que el grupo se sostiene presente para que todas las demás puedan recordarlo. Y entonces de golpe el grupo ya anda por ahí, por varias memorias, y es una cuestión de paciencia, de coordinación de tiempos y espacios, es una cuestión de ganas y de curiosidades para que el grupo se componga.<br>Y en esto andamos, componiendo, sosteniendo los grupos que nos crecen... y a eso los invitamos también, porque los grupos también crecen por los destiempos digitales...</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/que-son-los-grupos.jpg')) }}" class="card-img img-fluid" width="280" height="392" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">¿Qué son los grupos?</h2>
                    <div id="articleText2" class="article-text d-none">
                        <p class="text-justify">Son grupos que se mantienen y arman otros grupos... en principio de lectura, de charla, de pensamiento... pero también de cualquier otra cosa que permita armar un grupo.</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/quien-puede-participar.jpg')) }}" class="card-img img-fluid" width="280" height="246" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">¿Quién puede participar?</h2>
                    <div id="articleText3" class="article-text d-none">
                        <p class="text-justify">Cualquiera, pero <em>cualquiera</em> no es cualquier cosa... Hoy definimos los grupos como una organización de los <em>cualquiera</em>... el principio que nos conmueve es: <em>Cualquiera</em> puede participar de los grupos... pero solamente en tanto que <em>cualquiera</em>.</p>
                        <p class="text-justify">A ver... aclaren esto. ¿Si soy un estudiante puedo participar de los grupos? Sí, pero no en tanto estudiante, sino en tanto cualquiera. Es claro que para ser un estudiante hay que ser cualquiera... por lo tanto ser un estudiante no es un problema, en la medida en que uno acepte no ir a los grupos como estudiante sino como cualquiera. ¿Y si soy un profesor? Si sos un profesor podrás venir a los grupos en tanto cualquiera, pero no en tanto profesor.&nbsp; ¿Y si soy un militante? Podrás venir a los grupos en tanto cualquiera, pero no en tanto militante. ¿Y si soy un !!!!!!!!!? (La respuesta será la misma). Pueden venir a los grupos en tanto que cualquiera, pero no en tanto que !!!!!!!.</p>
                        <p class="text-justify">Tampoco nos gusta la trampa de la potencialidad. Es decir, confundir el cualquiera con un potencial. Por ejemplo, “cualquiera puede ser bombero”, “cualquiera puede ser héroe”. No nos referimos a ese cualquiera. No es un cualquiera que está esperando una determinación o una definición... Cualquiera no es un indefinido. Por eso los grupos no pueden transformarse en uno de esos sujetos sin dejar de ser los grupos. Si los que participamos de los grupos nos volviéramos todos profesores (en los grupos) sería un grupo de profesores y ya no podría venir cualquiera, o en todo caso ese cualquiera tendría que soportar a un grupo de profesores... y eso no se lo deseamos a nadie... tampoco queremos volvernos militantes...ni todos bomberos ni todos ciclistas, ni vegetarianos ni cantantes... ni... bueno, se entendió ¿No?</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/como-puedo-participar.jpg')) }}" class="card-img img-fluid" width="280" height="210" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">¿Cómo puedo participar?</h2>
                    <div id="articleText4" class="article-text d-none">
                        <p class="text-justify">La participación depende de lugar en el que se encuentren, del tiempo que dispongan....</p>
                        <p class="text-justify">1) <strong>Están en Buenos Aires (Argentina) y disponen de tiempo,</strong><br> sencillamente pueden venir a los grupos... nos mandan un mail acá: <a href="{{ route('contacto.index') }}">Contacto</a> contándonos un poco en que andan y a que grupo quieren ir. Esperan la respuesta.... y listo</p>
                        <p class="text-justify">2) <strong>Están en Buenos Aires y no disponen de tiempo o están lejos de Buenos Aires y no pueden participar presencialmente de los grupos</strong>.</p>
                        <p class="text-justify">Aquí hay varias opciones. Pueden</p>
                        <p class="text-justify"><strong>a) </strong>Leer allí donde estén lo que nosotros estemos leyendo en alguno de los grupos y luego escribirnos algo al respecto... para que nosotros podamos responderlo y así...</p>
                        <p class="text-justify"><strong>b) </strong>Proponer algún problema para pensar con nosotros... enviando algún escrito al respecto... para que nosotros podamos responder y así...</p>
                        <p class="text-justify"><strong>c) </strong>Por cierto, la mejor de todas: armar un grupo allí donde ustedes se encuentren con gente de por allí y ver de que manera podemos enlazarlo con nuestros grupos.</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/y-para-donde-van-con-esto.jpg')) }}" class="card-img img-fluid" width="280" height="338" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">¿Y para dónde van con esto?</h2>
                    <div id="articleText5" class="article-text d-none">
                        <p class="text-justify">Nuestro problema no es a donde vamos... porque no se trata de irnos, sino de estar yéndonos. Nuestro problema es estar yéndonos del presente plano, llano y homogéneo que se traza en los efectos globales, que pasa fundando ese todo que es ningún lado, y que al pasar constituye los medios de comunicación, las tendencias, las viralizaciones, la existencia fática (y nunca fáctica) de las redes sociales, la insignificancia de los papers, la pantomima de la representación política, las consignas vacías y sus convocatorias.<br> Y esto no alcanza, porque el presente plano es una ola, y hay que estar yéndonos también de su reflujo, &nbsp;de esos trazos abstractos, estadísticos, de esas integraciones cotidianas del miedo y la victimización, de la violencia triste y la construcción de enemigos impotentes y estúpidos, de todos los “más vistos”, de las nubes de tags.<br> Pero hay que ir (o irse) despacio... si bien es imprescindible estar yéndonos, tratamos de esmerarnos en no irnos del todo (al menos no por mucho tiempo). No queremos cortar cierta relación con ese presente (no armamos comunidades en la montaña ni grupos cerrados, no soñamos con islas desiertas ni con inmunidades de ningún tipo). Lo imprescindible es estar yéndonos sin irse, como la regla de la mermelada de Alicia: nunca hoy, siempre ayer o mañana. Si nos soltáramos de este presente, si dejáramos de irnos y nos fuéramos, entonces algo se cerraría y ya no podríamos dejar entrar a cualquiera (aún sabiendo que siempre entra, pero si uno cierra entra como en el castillo del cuento de Poe, con la máscara y esas cosas), y de pronto descubriríamos por qué eso de los cualquiera era tan importante... que en última instancia no se trataba tanto de armar un grupo como de agrupar un cierto movimiento, una cierta velocidad... armar grupo con eso que no deja de llegar (siempre ayer) y a la vez no dejar de irnos (siempre mañana)...<br> Y el problema ni siquiera termina ahí... porque estar yéndonos tiene una complicación más: hay que tener cuidado de no volver...</p>
                        <p class="text-justify">EN PROCESO</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/sobre-la-cuestion-pecuniaria.jpg')) }}" class="card-img img-fluid" width="280" height="503" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">Sobre la cuestión pecuniaria</h2>
                    <div id="articleText6" class="article-text d-none">
                        <p class="text-justify">No damos cursos por dinero ni queremos que nos den cursos por dinero. No vendemos ni compramos conocimiento de ningún tipo ni en ningún formato: curso, librito, coordinación de grupos, etc. Cuando el conocimiento y el dinero se juntan nos genera desconfianza. Y esto se vuelve casi contradictorio: nos parece interesante que una persona (siempre entre otras o con otras... nadie piensa solo) obtenga dinero para poder seguir pensando, pero nos genera desconfianza cuando nos quiere vender su curso o su librito. Ambas cosas están tan cerca pero infinitamente lejos, tan lejos que suponen dos organizaciones contrarias del conocimiento. Y como ya dijimos, esta segunda organización nos resulta sospechosa. Así que no compramos ni vendemos nada. (Claro que si nos quieren regalar dinero para que sigamos organizando grupos lo aceptaremos encantados... pero siempre será a cambio de nada).</p>
                    </div>
                </div>
            </article>

            <article class="card index-article shadow-sm">
                <img src="{{ asset(mix('img/sobre-la-cantidad.jpg')) }}" class="card-img img-fluid" width="280" height="186" alt="">
                <div class="card-img-overlay">
                    <h2 class="card-title h6">Sobre la cantidad</h2>
                    <div id="articleText7" class="article-text d-none">
                        <p class="text-justify">¿Cuál es la cifra de «cualquiera»? o ¿cuántos podemos ser?</p>
                        <p class="text-justify">¿Cuántos somos? es una pregunta que repetimos... porque hay un número, una cantidad en la que ya no somos, ya no podemos ser... Un número por debajo, demasiado personal, el grupo pierde su punto ciego, sus dispersiones incalculables y se sectorializa... Un número por encima, que empieza a contar de nuevo, y en este contar lo incalculable se vuelve rumor, cuchicheo, aparición de pequeñas zonas desconectadas que intentan recuperar la tensión perdida en la simplificación de la cantidad...</p>
                        <p class="text-justify">Entonces, ¿cuántos somos? Demasiados para establecer una relación privada, personal... demasiados como para poder cerrarnos (siempre hay una cantidad (incalculable también) que forma parte y no)... Pero también muy pocos, demasiado pocos como para volvernos públicos, generales, una muestra o una tendencia...</p>
                        <p class="text-justify">¿Cuántos somos, entonces? ¿Cuál es la cifra de lo íntimo?</p>
                    </div>
                </div>
            </article>
        </div>
    </section>

    @if ($currentGroups->count())
        <hr class="hr">

        <h2 class="h6 font-weight-bold mb-3">Grupos actuales</h2>

        <div class="groups-grid">
            @foreach ($currentGroups as $group)
                <a class="card card-group-info" href="{{ route('groups.show', [$group, $group->slug]) }}">
                    <div class="row no-gutters">
                        @if ($group->image)
                            <div class="col-md-4 col-img">
                                <div class="card-bg-image" style="background-image: url('{{ $group->imageUrl }}')"></div>
                            </div>
                        @endif
                        <div class="col-md-8 d-flex align-items-center">
                            <div class="card-body">
                                <h3 class="card-title">{{ $group->title }}</h3>
                                @if ($group->schedule || $group->place)
                                    <div class="group-info text-center text-muted pt-2">
                                        @if ($group->schedule)
                                            <span class="group-info-item">
                                                <i class="fal fa-calendar-alt"></i>
                                                {{ $group->schedule }}
                                            </span>
                                        @endif
                                        @if ($group->place)
                                            <span class="group-info-item">
                                                <i class="fal fa-map-marker-alt"></i>
                                                {{ $group->place }}
                                            </span>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    @endif

    <hr class="hr">

        <h2 class="h6 font-weight-bold mb-3">Contacto</h2>

        <div class="contact-grid">
            <p>Si querés descargarte algún libro de la página, leer los apuntes, sumarte a algún grupo o revisar nuestra página de escritura, podés mandar un mail por acá <a href = "{{ route('contacto.index') }}">Contacto</a>. Esperá la respuesta y vemos qué se puede hacer. </p>
        </div>
@endsection
