<!doctype html>
<html lang="es"@isset($class) class="{{ $class }}"@endisset>
<head>
    @include('includes.head')
</head>
<body>
<div id="app">
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    @include('includes.main-navbar')
    <div class="container py-4">
        <div class="row">
            <div class="col-12 col-md-8 col-xl-9">
                <main class="main-content">
                    @yield('content')
                </main>
            </div>
            @if(!Request::is('escritura', 'escritura/create'))
            <div class="col-12 col-md-4 col-xl-3">
                <aside class="main-sidebar">
                    @include('includes.sidebar')
                </aside>
            </div>
            @endif
        </div>
    </div>
    @include('includes.scripts')
</div>
</body>
</html>
