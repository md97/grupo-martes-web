<!doctype html>
<html lang="es">
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.main-navbar')
    <main class="container py-4 text-center">
        <h1 class="error-code">
            @yield('code', __('Oh no'))
            <small class="text-muted">@yield('message')</small>
        </h1>
        <a href="{{ route('index') }}" class="btn btn-outline-secondary">Inicio</a>
        <a href="{{ route('groups.index') }}" class="btn btn-outline-secondary mx-2">Grupos</a>
        <div class="btn-group">
            <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                Textos
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md-left">
                <a class="dropdown-item" href="{{ route('files.textos-prestados') }}">Textos prestados</a>
                <a class="dropdown-item" href="{{ route('files.textos-propios') }}">Textos propios</a>
                <a class="dropdown-item" href="{{ route('files.traducciones') }}">Traducciones</a>
                <a class="dropdown-item" href="{{ route('files.libros') }}">Libros</a>
            </div>
        </div>
    </main>
    @include('includes.scripts')
</body>
</html>
