<!doctype html>
<html lang="es"@isset($class) class="{{ $class }}"@endisset>
<head>
    @include('includes.head')
</head>
<body>
<div id="app">
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    @include('includes.main-navbar')
    <div class="container py-4">
        <div class="row">
                    @yield('content')
            </div>
        </div>
    </div>
    @include('includes.scripts')
</div>
</body>
</html>
