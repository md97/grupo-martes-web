<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Inicio
Route::get('/', 'HomeController@index')->name('index');

// Grupos
Route::get('grupos', 'GroupController@index')->name('groups.index');
Route::get('grupos/{group}/{slug}', 'GroupController@show')->name('groups.show');

// Descargas
Route::get('descargas/{download}/{name}', 'DownloadController@show')->name('downloads.show');

// Textos
Route::get('textos-prestados', 'FileController@index')->name('files.textos-prestados');
Route::get('textos-propios', 'FileController@index')->name('files.textos-propios');
Route::get('traducciones', 'FileController@index')->name('files.traducciones');
Route::get('libros', 'FileController@index')->name('files.libros');
Route::get('textos/{file}/{slug}', 'FileController@show')->name('files.show');

// Contacto
Route::get('contacto', 'ContactoController@index')->name('contacto.index');
Route::post('contacto/enviar', 'ContactoController@send')->name('contacto.enviar');

/**
 * Backend.
 */
Auth::routes(['register' => false, 'reset' => false, 'confirm' => false, 'verify' => false]);
Route::middleware('auth', 'admin')->namespace('Backend')->prefix('admin')->as('admin.')->group(function () {
    // Home
    Route::get('/', 'HomeController@index')->name('home');

    // Grupos
    Route::resource('groups', 'GroupController', ['except' => ['show']]);

    // Archivos
    Route::resource('files', 'FileController', ['except' => ['show']]);
});

// Escritura
Route::resource('escritura', 'EscrituraController');