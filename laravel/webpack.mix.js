const mix = require('laravel-mix');
require('laravel-mix-svg-vue');
require('laravel-mix-imagemin');

let path = require('path');
let publicPath = path.normalize('../public_html');
mix.setPublicPath(publicPath);

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'js')
    .sass('resources/sass/app.scss', 'css')
    .sass('resources/sass/main.scss', 'css')
    .scripts([
        'resources/js/_plugins-open.js',
        'node_modules/salvattore/dist/salvattore.min.js',
    ], publicPath + '/js/plugins.js')
    .svgVue()
    .imagemin(
        'img/*',
        {
            context: 'resources'
        },
        {
            plugins: [
                require('imagemin-mozjpeg')({
                    quality: 90,
                    progressive: true,
                }),
            ],
        }
    );

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps(true, 'source-map');
}
